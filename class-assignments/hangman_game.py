# Name: Md. Rafayet Ullah
# ID: 181910
# Batch: MIT 19th

# Python Version: 2.7.11


#### WORD PREDICTION HANGMAN GAME ####

from random import randint

# list of easy english words to be randomly selected by program to guess
bag_of_words = ["angry", "bright", "clean", "complete", "equal",
                "electric", "general", "hard",  "healthy", "important",
                "medical", "military", "necessary", "physical", "possible",
                "regular", "second", "separate", "special", "straight"]


# predefined variables
chance = 10
play_game = True


# messages to be printed different stages of the game
result = """{answer}
Chances Left: {chance}"""

congrats_msg = """!!! CONGRATULATION !!!
You Saved The Man From Hanging
Your Guess Word Is: {word}
You WON The Game Loosing Only {chance} Chances.
"""

game_over_msg = """!!! GAME OVER !!!
Man Hanged
Your Guess Word Is: {word}
"""

choice_msg = """Please Enter Your Choice:
    1. Play Game Again
    2. Exit Game
Choice: """



# loop will continue until player ends the game and quit to play
while play_game:
    game_chance = chance # reset game_chance every time player start guess game
    quiz_word = bag_of_words[randint(0, len(bag_of_words) - 1)].upper() # random quiz word selected by program from bag_of_words
    answer = ["*" for w in quiz_word] # temp list to set correct letter position

    # guess game started
    while game_chance != 0 and "*" in answer:
        print(result.format(answer=" ".join(answer),
                            chance=game_chance)) # prints game condition message

        letter = raw_input("Enter Guess Letter: ").upper()[0] # received input from player and take only first letter if multiple inserted

        # determines position of letter in guess_word and replaces in answer and handles game_chance
        if letter in quiz_word and letter not in answer:
            for i, w in enumerate(quiz_word):
                if w == letter:
                    answer[i] = letter
        else:
            game_chance -= 1

    # if won the game then prints congrats_msg and if lost then prints game_over_msg
    if "*" not in answer:
        print(congrats_msg.format(
            word=" ".join(answer),
            chance=chance-game_chance
        )) # prints congrats_msg 
    else:
        print(game_over_msg.format(word=" ".join(list(quiz_word)))) # prints game_over_msg

    # loop continues until player insert valid input
    while True:
        choice = raw_input(choice_msg) # take player input to play game or exit game
        if choice.isdigit():
            choice = int(choice)
        if choice==1:
            play_game = True
            break
        elif choice==2:
            play_game = False
            break
        else:
            continue

